# API Ball Game

This game demonstrate the iteration of APIs. Play with the balls by just sending http request

## Installation

Build the application `mvn clean package`. Deploy the .zip file in any Mule Runtime

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## API Simple call

- Return List of balls

  GET  localhost:8081/api-ball-game/balls  Header:  Content-Type = application/json
  
- Create a new ball

  POST localhost:8081/api-ball-game/balls  Header:  Content-Type = application/json
  
  BODY Example:
  
  {
    "posX": 5,
    "posY": 200,
    "color": "BLACK"
  }
  
- Delete an existing ball

  DELETE  localhost:8081/api-ball-game/balls/{ball_color}  Header:  Content-Type = application/json
  
- Move a ball

  Patch   localhost:8081/api-ball-game/balls/{ball_color}  Header:  Content-Type = application/json
   
  BODY Example:
  
  {
    "posX": 5,
    "posY": 200
  }
  